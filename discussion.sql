===================
SQL CRUD OPERATIONS
===================

------ >> (CREATE)
 Adding a record:
 Syntax
 	INSERT INTO table_name (column_name) VALUES (values1);
 Example
 	INSERT INTO artist (name) VALUES ("Nirvana");
 	INSERT INTO artist (name) VALUES ("Emma Watsons");
 	INSERT INTO artist (name) VALUES ("Orlando Bloom");


 Adding record with multiple columns:
 Syntax
 	INSERT INTO table_name (column_name, column_name) VALUES (value1, value2)
 Example
 	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Nevermind", "1991-08-24", 1);
 	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("I will sing", "2008-09-21", 2);
 	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Hillsong", "2020-08-10", 3);


Adding multiple records:
Syntax
 	INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);
Example
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Smell Like a Teen Spirit", 501, "Grunge", 1);
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Be magnified", 402, "worship", 2);
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("All for love", 523, "praise", 3);




------ >> (READ)
Show all records:
Syntax
	SELECT column_name FROM table_name;
Example
	SELECT name FROM artist;


Show records with selected columns:
Syntax
	SELECT (column_name1, column_name2) FROM table_name;

Example
	SELECT song_name FROM songs;
	SELECT song_name, genre FROM songs;
	SELECT album_title, date_released FROM albums;

	Show full table:
	SELECT * FROM songs;


Show records that meet certain condition (filtering out):
Syntax:
	SELECT column_name FROM table_name WHERE condition;
Example:
	SELECT song_name FROM songs WHERE genre = "worship";



Display records with multiple conditions:
Syntax
	AND Clause
	SELECT column_name FROM table_name WHERE condition1 AND condition2;

	AND Clause
	SELECT column_name FROM table_name WHERE condition1 OR condition2;

Example
(SPECIFIC COLUMNS)
	SELECT song_name, length FROM songs WHERE length > 314 OR genre = "Grunge";	

	SELECT song_name, length FROM songs WHERE length > 314 AND genre = "worship";	

(WHOLE TABLE)
	SELECT * FROM songs WHERE length > 350 AND genre = "Grunge";



------ >> (UPDATE)
Syntax
	INSERT INTO artist (name) VALUES ("Incubus");
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Monuments and Melodies", "2009-06-16", 4);
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 4);

-- ALTER TABLE table_name AUTO_INCREMENT = 1; >> reset auto increment

Updating records:
Syntax
	UPDATE table_name SET column_name = value WHERE condition;
	UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;

Example
	UPDATE songs SET genre = "Rock" WHERE length > 501;
	UPDATE songs SET length = 240 WHERE song_name = "All for love";





-------- >> (DELETE)
Syntax
DELETE FROM table_name WHERE condition;

Example
	DELETE FROM songs WHERE genre = "ROCK" AND length > 400;



(MINI ACTIVITY) 
tasksdb

INSERT INTO users (username, password) VALUES ("Theoon", "pw1");
INSERT INTO users (username, password) VALUES ("Granger", "pw2");


INSERT INTO tasks (status, name, users_id) VALUES ("finished", "play games", 1);
INSERT INTO tasks (status, name, users_id) VALUES ("finished", "cook noodles", 1);


INSERT INTO tasks (status, name, users_id) VALUES ("pending", "record songs", 2);
INSERT INTO tasks (status, name, users_id) VALUES ("pending", "write poem", 2);


SELECT * FROM users;
SELECT * FROM tasks;















